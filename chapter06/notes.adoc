= Compiling with optimization
:sectnums:
:toc:

== Source-level optimization

=== Common subexpression elimination

=== Function inlining

== Speed-space tradeoffs

=== Loop unrolling

== Scheduling

== Optimization levels

[quote]
____
-O0 or no -O option (default)
At this optimization level GCC does not perform any optimization and compiles
the source code in the most straightforward way possible. Each command
in the source code is converted directly to the corresponding instructions in
the executable file, without rearrangement. This is the best option to use
when debugging a program and is the default if no optimization level option is
specified.

-Os
This option selects optimizations which reduce the size of an executable. The
aim of this option is to produce the smallest possible executable, for systems
constrained by memory or disk space. In some cases a smaller executable will
also run faster, due to better cache usage.
____

== Optimization and debugging

== Optimization and compiler warnings

[quote]
____
The -Wuninitialized option (which is included in -Wall) warns about variables that
are read without being initialized. It only works when the program is compiled with opti-
mization, so that data-flow analysis is enabled.
____

[source,c]
----
include::example/sign.c[]
----

[source]
----
$ type c11
c11 is an alias for gcc -std=c11 -pedantic -Wall -Wextra

$ c11 -O2 sign.c
sign.c: In function ‘sign’:
sign.c:8:12: warning: ‘s’ may be used uninitialized in this function [-Wmaybe-uninitialized]
    8 |     return s;
      |            ^
/usr/bin/ld: /usr/lib/gcc/x86_64-linux-gnu/9/../../../x86_64-linux-gnu/Scrt1.o: in function `_start':
(.text+0x24): undefined reference to `main'
collect2: error: ld returned 1 exit status

$ c11 -c sign.c

----
