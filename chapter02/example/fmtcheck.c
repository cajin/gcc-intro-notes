#include <stdio.h>

int main(void)
{
    printf("%d\n", 1Ul);

    const char *const fmt = "%d\n";
    printf(fmt, 1UL);
    return 0;
}
